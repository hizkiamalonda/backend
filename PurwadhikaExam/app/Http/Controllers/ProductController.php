<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnitRumah;						// untuk memakai model nya
use Illuminate\Support\Facades\DB;		// untuk memakai metode eloquent ORM

class ProductController extends Controller
{
    function CreateUnit(Request $request)				// request untuk ngambil data untuk disimpan di database
    {
    	// membungkus insert DB sampai commit, kalau salah satu gagal lgsg di rollback dalam catch.
    	DB::beginTransaction();						

    	try
    	{
    		

    		// ngambil data yg dikirim dari client dan 'name' adalah key dari data json yg dimasukin client.

    		$kav = $request->input('kavling');				
            $blok = $request->input('blok');
            $nomor = $request->input('nomor_rumah');
            $harga = $request->input('harga_rumah');
            $tanah = $request->input('luas_tanah');
            $bangunan = $request->input('luas_bangunan');
            $customer = $request->input('customer_id');

    		// untuk nge save ke database

    		$unit = new UnitRumah;
    		$unit->kavling = $kav;
    		$unit->blok = $blok;
    		$unit->nomor_rumah = $nomor;
    		$unit->harga_rumah = $harga;
    		$unit->luas_tanah = $tanah;
    		$unit->luas_bangunan = $bangunan;
            $unit->customer_id = $customer;

    		$unit->save();                                        // // untuk menyimpan data pada database

            $data = UnitRumah::get();

    		DB::commit();

    		return response()->json($data, 200);				// untuk verifikasi berhasil
    	}
    	
    	catch(\Exception $e)
    	{
    		DB::rollback();
    		return response()->json(["message" => $e->getMessage()], 500);			// untuk verikasi gagal
    	}
    }

    function DeleteUnit(Request $request){

    	$id = $request->input("id");
    	DB::delete('delete from Unit where id = ?', [$id]);
    }
}
