<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'unit'] , function()			//	localhost:8000/api/unit
{
	Route::get('/' , function(){
		$unit = DB::table('Unit')->get();
		return response()->json($unit, 200);
	});	

	Route::post('/create' , 'ProductController@CreateUnit');			
	Route::post('/delete' , 'ProductController@DeleteUnit');	
});


